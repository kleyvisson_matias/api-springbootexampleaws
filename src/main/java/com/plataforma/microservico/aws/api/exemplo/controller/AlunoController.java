package com.plataforma.microservico.aws.api.exemplo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlunoController {
	
	@GetMapping
	@RequestMapping(value = "/aluno")
	public String aluno() {
		return "Kley";
	}
}
